import {writable} from "svelte/store";
import {persist, createSessionStorage} from "@macfja/svelte-persistent-store";
export const activePage = persist(writable(''),createSessionStorage(),'selectedPage');