

export const index = 1;
let component_cache;
export const component = async () => component_cache ??= (await import('../entries/fallbacks/error.svelte.js')).default;
export const imports = ["_app/immutable/nodes/1.Amfnq3m1.js","_app/immutable/chunks/scheduler.rGTiESYz.js","_app/immutable/chunks/index.bz4RYtYs.js","_app/immutable/chunks/entry.aRRrO6cc.js","_app/immutable/chunks/index.q1i6dmSR.js"];
export const stylesheets = [];
export const fonts = [];
