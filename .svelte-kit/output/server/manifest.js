export const manifest = (() => {
function __memo(fn) {
	let value;
	return () => value ??= (value = fn());
}

return {
	appDir: "_app",
	appPath: "_app",
	assets: new Set(["favicon.png","fonts/noto-sans-lao/NotoSansLao-Black.woff","fonts/noto-sans-lao/NotoSansLao-Black.woff2","fonts/noto-sans-lao/NotoSansLao-Bold.woff","fonts/noto-sans-lao/NotoSansLao-Bold.woff2","fonts/noto-sans-lao/NotoSansLao-ExtraBold.woff","fonts/noto-sans-lao/NotoSansLao-ExtraBold.woff2","fonts/noto-sans-lao/NotoSansLao-ExtraLight.woff","fonts/noto-sans-lao/NotoSansLao-ExtraLight.woff2","fonts/noto-sans-lao/NotoSansLao-Light.woff","fonts/noto-sans-lao/NotoSansLao-Light.woff2","fonts/noto-sans-lao/NotoSansLao-Medium.woff","fonts/noto-sans-lao/NotoSansLao-Medium.woff2","fonts/noto-sans-lao/NotoSansLao-Regular.woff","fonts/noto-sans-lao/NotoSansLao-Regular.woff2","fonts/noto-sans-lao/NotoSansLao-SemiBold.woff","fonts/noto-sans-lao/NotoSansLao-SemiBold.woff2","fonts/noto-sans-lao/NotoSansLao-Thin.woff","fonts/noto-sans-lao/NotoSansLao-Thin.woff2","fonts/noto-sans-lao/demo.html","fonts/noto-sans-lao/stylesheet.css","images/about_bg_01.png","images/about_img_01.png","images/about_img_02.png","images/about_img_03.png","images/home_img_01.png","images/home_img_02.png","images/service_img_01.png","robots.txt","svg/close.svg","svg/option.svg"]),
	mimeTypes: {".png":"image/png",".woff":"font/woff",".woff2":"font/woff2",".html":"text/html",".css":"text/css",".txt":"text/plain",".svg":"image/svg+xml"},
	_: {
		client: {"start":"_app/immutable/entry/start.YKDrEVZq.js","app":"_app/immutable/entry/app.VWgaU1Kn.js","imports":["_app/immutable/entry/start.YKDrEVZq.js","_app/immutable/chunks/entry.aRRrO6cc.js","_app/immutable/chunks/scheduler.rGTiESYz.js","_app/immutable/chunks/index.q1i6dmSR.js","_app/immutable/entry/app.VWgaU1Kn.js","_app/immutable/chunks/scheduler.rGTiESYz.js","_app/immutable/chunks/index.bz4RYtYs.js"],"stylesheets":[],"fonts":[],"uses_env_dynamic_public":false},
		nodes: [
			__memo(() => import('./nodes/0.js')),
			__memo(() => import('./nodes/1.js'))
		],
		routes: [
			
		],
		matchers: async () => {
			
			return {  };
		},
		server_assets: {}
	}
}
})();
