import * as universal from '../entries/pages/about/_page.ts.js';

export const index = 3;
let component_cache;
export const component = async () => component_cache ??= (await import('../entries/pages/about/_page.svelte.js')).default;
export { universal };
export const universal_id = "src/routes/about/+page.ts";
export const imports = ["_app/immutable/nodes/3.V0L2FkvY.js","_app/immutable/chunks/scheduler.rGTiESYz.js","_app/immutable/chunks/index.bz4RYtYs.js","_app/immutable/chunks/Footer.kPYS5wGb.js"];
export const stylesheets = [];
export const fonts = [];
